/*
 * Odczytywanie wyboru opcji programu przez u�ytkownika 
 */
package utils;

import java.util.Scanner;

import data.Dog;

public class DataReader {
	private Scanner sc;
	
	public DataReader() {
		sc=new Scanner(System.in);
	}
	
	public void close() {
		sc.close();
	}
	
	public int getInt() {
		int number = sc.nextInt();
		sc.nextLine(); 
		return number;
	}
	
	
	public Dog readAndCreateDog() {
		
		System.out.println("Imi� psa: ");
		String name=sc.nextLine();
		System.out.println("Rasa psa: ");
		String breed = sc.nextLine();
		return new Dog(name, breed);
	}

}
