/*
 * Klasa odpowiedzialna za przechywanie danych o psach
 */
package data;

import data.Dog;;

public class Pound {
	
	private static final int MAX_DOG = 100;
	private Dog [] dogs;
	private int dogsNumber;
	
	//getter i setter
	public Dog [] getDogs() {
		return dogs;
	}
	public void setDogs(Dog [] dogs) {
		this.dogs = dogs;
	}
	public int getDogsNumber() {
		return dogsNumber;
	}
	public void setDogsNumber(int dogsNumber) {
		this.dogsNumber = dogsNumber;
	}
	
	//konstruktor
	public Pound () {
		dogs = new Dog[MAX_DOG];
		setDogsNumber(0); //czy ustawia� to na 0???????????????????
	}
	
	//dodawanie nowego psa
	public void addDog (Dog dog) {
		if (dogsNumber < MAX_DOG) {
			dogs[dogsNumber]=dog;
			dogsNumber++;
		}else {
			System.out.println("Wi�cej zwierzak�w si� nie zmie�ci!");
		}
	}
	
	//wy�wietlanie informacji o wszystkich psach w schronisku
	public void printDogs() {
		if (dogsNumber==0) {
			System.out.println("Brak zwierzak�w w schronisku");
		}
		for (int i=0; i<dogsNumber; i++) {
			System.out.println((i+1)+". "+ dogs[i]);
		}
	}
	
	public void deleteDogs(int deleteNumber) {
		dogs[deleteNumber-1]=dogs[deleteNumber];
		dogsNumber--;
	}
	
	//ile miejsc zosta�o?
	public void freePlaces() {
		int place = MAX_DOG-getDogsNumber();
		System.out.println("Pozosta�o " + place + " wolnych miejsc w schronisku.");
	}
	
	
	
	
}
