package data;

public class Dog {
	private String name; //imi� 
	private String breed; //rasa
	
	//getter i setter
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBreed() {
		return breed;
	}
	public void setBreed(String breed) {
		this.breed = breed;
	}
	
	//konstruktor
	public Dog (String name, String breed) {
		setName(name);
		setBreed(breed);
	}

	@Override
	public String toString() {
		return name + ", rasa: "+breed;
	}
	
}
