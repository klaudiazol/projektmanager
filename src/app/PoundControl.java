/*
 * Klasa odpowiedzialna za sterowanie aplikacj�
 */
		
package app;

import data.Dog;
import data.Pound;
import utils.DataReader;

public class PoundControl {

	public static final int EXIT = 0;
	public static final int ADD_DOG = 1;
	public static final int PRINT_DOGS = 2;
	public static final int DELETE_DOGS = 3;
	public static final int STATUS = 4;
	
	//zmienna do komunikacji z u�ytkownikiem
	private DataReader dataReader;
	
	private Pound pound;
	
	//konstruktor
	public PoundControl() {
		dataReader = new DataReader();
		pound=new Pound();
	}
	
	//g��wna p�tla programu
	public void controlLoop() {
		int option;
		printOptions();
		while ((option=dataReader.getInt())!=EXIT) {
			switch (option) {
			case ADD_DOG:
				addDog();
				break;
			case PRINT_DOGS:
				printDogs();
				break;
			case DELETE_DOGS:
				printDogs();
				deleteDogs();
				break;
			case STATUS:
				status();
				break;
			default:
				System.out.println("Nie ma takiej opcji, wprowadz ponownie.");
			}
		printOptions();
		}
	}

	
	
	private void printOptions() {
		System.out.println("Wybierz opcj�: ");
		System.out.println("0 - wyj�cie z programu");
		System.out.println("1 - dodanie nowego zwierzaka");
		System.out.println("2 - wy�wietl wszystkie zwierzaki ze schroniska");		
		System.out.println("3 - usu� wybranego zwierzaka ze schroniska");		
		System.out.println("4 - ile miejsc jeszcze zosta�o?");		
	}
	
	//ADD_DOG
	private void addDog() {
		Dog dog = dataReader.readAndCreateDog();
		pound.addDog(dog);
	}
	
	//PRINT_DOGS
	private void printDogs() {
		pound.printDogs();
	}
	
	//DELETE_DOGS
	private void deleteDogs() {
		if (pound.getDogsNumber()!=0) {
			System.out.println("Kt�rego zwierzaka usuna�? Podaj numer.");
			int deleteNumber=dataReader.getInt();
			if (deleteNumber>0 && deleteNumber<=pound.getDogsNumber()) {
				while (deleteNumber<=pound.getDogsNumber()) {
					pound.deleteDogs(deleteNumber);
					deleteNumber++;
					}
				printDogs();
			}
		}
	}
	
	//STATUS
	private void status() {
		pound.freePlaces();
	}
	
	
	}


